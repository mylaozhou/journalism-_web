from flask import render_template, current_app, session, request, jsonify, g

from info import constants
from info.models import User, News, Category
from info.utils.common import user_login_data
from info.utils.response_code import RET

from . import vs_index


# add the root index of view
@vs_index.route("/")
@user_login_data
def index():
    # get the current user id
    # user_id = session.get("user_id")

    # get information by user_id
    # user = None  # type: User
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.logger(e)

    # get ranking data
    news_list = None
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    # click_news_list = []
    # for news in news_list if news_list else []:
    #     click_news_list.append(news.to_basic_dict())
    click_news_list = [news.to_basic_dict() for news in news_list]

    # get news categories data
    categories = Category.query.all()

    #get the list of categories dict from db
    categories_dict_list = [category.to_dict() for category in categories]

    data = {
        "user_info": g.user.to_dict() if g.user else None,
        "click_news_list": click_news_list,
        "categories": categories_dict_list
    }
    # print(len(click_news_list), click_news_list[0])  # for test

    return render_template("news/index.html", data=data)


@vs_index.route("/newslist")
def news_list():
    """
    1. get params
    2. verify params
    3. query data
    4. return data
    :return:
    """
    # 1. get params
    args_dict = request.args
    page = args_dict.get("page", "1")
    per_page = args_dict.get("per_page", constants.HOME_PAGE_MAX_NEWS)
    category_id = args_dict.get("cid", "1")

    # 2. verify params
    try:
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="paramsError")

    # 3. query data
    filters = [News.status == 0]
    # if category_id isn't 1, add filter
    if category_id != "1":
        filters.append(News.category_id  == category_id)

    try:
        paginate_items = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)

        page_items = paginate_items.items
        total_page = paginate_items.pages
        current_page = paginate_items.page
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="dbQueryError")

    news_li = [news.to_dict() for news in page_items]

    data = {
        "totalPage": total_page,
        "newsList": news_li,
        "currentPage": current_page,
        "cid": category_id
    }

    # return jsonify(
    #         errno=RET.OK, errmsg="OK",
    #         totalPage=total_page, currentPage=current_page,
    #         newsList=news_li, cid=category_id
    #     )

    return jsonify(errno=RET.OK, errmsg="OK", data=data)


@vs_index.route("/favicon.ico")
def favicon():
    return current_app.send_static_file("news/favicon1.ico")
