import logging
from logging.handlers import RotatingFileHandler

from flask import Flask, session, render_template, g
from redis import StrictRedis
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
# 可以用来指定 session 保存的位置
from flask_session import Session

from config import config
from info.utils.common import user_login_data

db = SQLAlchemy()
redis_store = None




def create_app(config_name):
    """通过传入不同的配置名字，初始化其对应配置的应用实列"""


    setup_log(config_name)
    app = Flask(__name__)

    # 加载配置
    app.config.from_object(config[config_name])

    db.init_app(app)

    # 初始化 redis 存储对象
    global redis_store

    redis_store = StrictRedis(host=config[config_name].REDIS_HOST ,port=config[config_name].REDIS_PORT,decode_responses=True)

    # 开启当前项目 CSRF 保护
    # CSRFProtect(app)

    # 设置session保存指定位置
    Session(app)
    from .modules.index import vs_index
    app.register_blueprint(vs_index)

    from info.modules.passport import pspr_blu
    app.register_blueprint(pspr_blu)

    from info.modules.news import news_blu
    app.register_blueprint(news_blu)

    from info.utils.common import do_index_class
    # 添加自定义过滤器
    app.add_template_filter(do_index_class, "indexClass")

    # 用户信息蓝图
    from info.modules.profile import profile_blu
    app.register_blueprint(profile_blu)

    # 管理员蓝图
    from info.modules.admin import admin_biu
    app.register_blueprint(admin_biu)

    @app.errorhandler(404)
    @user_login_data
    def page_not_found(_):
        user = g.user
        data = {"user_info":user.to_dict() if user else None}
        return render_template('news/404.html',data=data)
    return app


def setup_log(config_name):
    """配置日志"""

    # 设置日志的记录等级
    logging.basicConfig(level=config[config_name].LDG_LEVEL) # 调试debug等级

    # 创建日志记录器，指明日志保存的路径 每个日志文件的最大大小，保存的日志文件上限
    file_log_handler = RotatingFileHandler('logs/log',maxBytes=1024*1024*100,backupCount=10)

    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志等级
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')

    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)

    # 为全局的日志工具对象 （flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


