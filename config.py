import logging
from redis import StrictRedis


class Config(object):

    SECRET_KEY = "HTF78+8ddrQ4W+rsj7CCT4WLuxurlq8Gu0P9hKnRw8rLEME+zJAJ1t20PA3cAPsL"

    # mysql 配置
    SQLALCHEMY_DATABASE_URI ='mysql://root:mysql@127.0.0.1:3306/journalism_web'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # redis 配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # Session 保存配置
    SESSION_TYPE = "redis"

    # 开启session签名
    SESSION_USE_SIGNER = True

    # 指定 Session 保存的 redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)

    # 设置需要过期
    SESSION_PERMAMENT = False

    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2


class DevelopementConfig(Config):
    """开发模式下的配置"""
    DEBUG = True
    LDG_LEVEL = logging.DEBUG

class ProductionConfig(Config):
    """生产模式下的配置"""
    LDG_LEVEL = logging.ERROR


config = {
    "development": DevelopementConfig,
    "production": ProductionConfig
}
